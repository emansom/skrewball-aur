# AUR Packages

Package sources for all the AUR packages I [maintain](https://aur.archlinux.org/packages?K=skrewball&SeB=m) and [co-maintain](https://aur.archlinux.org/packages?K=skrewball&SeB=c) for the Arch Linux User Repository.

## Install

Install AUR packages with your preferred AUR Helper, for example:

```
paru -S package-name
```

Manual Installation:

```
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/package-name.git
cd package-name
makepkg -si
```

## Package Versions

<!--[![%s](https://repology.org/badge/version-for-repo/aur/%s.svg?header=%s)](https://repology.org/project/%s/versions)-->
[![pirate](https://repology.org/badge/version-for-repo/aur/pirate.svg?header=pirate)](https://repology.org/project/pirate/versions)\
[![treasure-chest](https://repology.org/badge/version-for-repo/aur/treasure-chest.svg?header=treasure-chest-bin)](https://repology.org/project/treasure-chest/versions)\
[![asus-ec-sensors-dkms](https://repology.org/badge/version-for-repo/aur/asus-ec-sensors-dkms.svg?header=asus-ec-sensors-dkms)](https://repology.org/project/asus-ec-sensors-dkms/versions)\
[![gnome:openweather](https://repology.org/badge/version-for-repo/aur/gnome:openweather.svg?header=gnome:openweather)](https://repology.org/project/gnome:openweather/versions)\
[![gnome:app-icons-taskbar](https://repology.org/badge/version-for-repo/aur/gnome:app-icons-taskbar.svg?header=gnome:app-icons-taskbar)](https://repology.org/project/gnome:app-icons-taskbar/versions)\
[![gnome:blur-my-shell](https://repology.org/badge/version-for-repo/aur/gnome:blur-my-shell.svg?header=gnome:blur-my-shell)](https://repology.org/project/gnome:blur-my-shell/versions)\
[![gnome:clipboard-history](https://repology.org/badge/version-for-repo/aur/gnome:clipboard-history.svg?header=gnome:clipboard-history)](https://repology.org/project/gnome:clipboard-history/versions)\
[![gnome:color-picker](https://repology.org/badge/version-for-repo/aur/gnome:color-picker.svg?header=gnome:color-picker)](https://repology.org/project/gnome:color-picker/versions)\
[![gnome:hide-universal-access](https://repology.org/badge/version-for-repo/aur/gnome:hide-universal-access.svg?header=gnome:hide-universal-access)](https://repology.org/project/gnome:hide-universal-access/versions)\
[![firefox-gnome-theme](https://repology.org/badge/version-for-repo/aur/firefox-gnome-theme.svg?header=firefox-gnome-theme)](https://repology.org/project/firefox-gnome-theme/versions)

# AUR Scripts

The AUR scripts `aur_upload.sh`, `init_or_update.sh`, `new_package.sh`, and `remove_package.sh` are fetched from [mschubert's repo](https://github.com/mschubert/PKGBUILDs). So a huge thanks goes to Michael for these!

These scripts simplify using `git subtree` to maintain each package in this repository and at the same time push to the AUR.

## Committing

Just use a normal git commit, but be sure to commit changes to each PKGBUILD directory separately.

## Uploading to AUR

This pushes packages to both AUR (subtree) and Gitlab (full repository).

```
./aur_upload.sh <pkgname(s)>
```

## Adding a new package

This will use a `git clone` hook to [register a new package with the AUR](https://wiki.archlinux.org/title/AUR_submission_guidelines#Submitting_packages).

```
./new_package <pkgname(s)>
```

## Removing a package

This will remove the local directory and the package remote. To fully delete the package, [file a deletion request](https://wiki.archlinux.org/title/AUR_submission_guidelines#Requests).

```
./remove_package <pkgname(s)>
```
